package com.paleservice.paleservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.paleservice.jpa.resource")
@Configuration
public class Config {

}
