package com.paleservice.paleservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaleServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaleServiceApplication.class, args);
	}
}
